create database "dbconntest";
create user "dbconntest" with encrypted password 'dbconntest';
alter role "dbconntest" createdb;
--grant all privileges on database "dbconntest" to "dbconntest";
